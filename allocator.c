//  memory allocator
//  allocator.c ... implementation
//

#include "allocator.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define HEADER_SIZE    sizeof(struct free_list_header)  
#define MAGIC_FREE     0xDEADBEEF
#define MAGIC_ALLOC    0xBEEFDEAD
#define TRUE           1
#define FALSE          0

typedef unsigned char byte;
typedef u_int32_t vlink_t;
typedef u_int32_t vsize_t;
typedef u_int32_t vaddr_t;

typedef struct free_list_header {
   u_int32_t magic;  // ought to contain MAGIC_FREE
   vsize_t size;     // # bytes in this block (including header)
   vlink_t next;     // memory[] index of next free block
   vlink_t prev;     // memory[] index of previous free block
} free_header_t;

// Global data

static byte *memory = NULL;   // pointer to start of allocator memory
static vaddr_t free_list_ptr; // index in memory[] of first block in free list
static vsize_t memory_size;   // number of bytes malloc'd in memory[]

void mergeNode(void);
void halveNode(free_header_t *curr);
u_int32_t memoryInit (u_int32_t size); //returns the size of the next pow 2 for memory allocation
u_int32_t degreeTwo (u_int32_t size, u_int32_t memory); //finds out how many times you have to divide the memory by 2
vaddr_t convertPointer (free_header_t *pointer); //converts a pointer into a corresponding index value
free_header_t *convertIndex (vaddr_t index); //converts an index into the corresponding pointer value




// Input: size - number of bytes to make available to the allocator
// Output: none              
// Precondition: Size is a power of two.
// Postcondition: `size` bytes are now available to the allocator
// 
// (If the allocator is already initialised, this function does nothing,
//  even if it was initialised with different size)

void vlad_init(u_int32_t size){
    
    // Record the size of malloc'd memory
    memory_size = memoryInit(size) < 512 ? 512 : memoryInit(size);
    
    // Allocate initial memory
    memory = malloc(memory_size * sizeof(byte));
    
    // Abort if malloc fails
    if(memory == NULL){
        fprintf(stderr, "vlad_init: insufficient memory");
        abort();
    }
    
    free_list_ptr = 0;
    
    // Declare free_list_header
    free_header_t *list = (free_header_t *)&memory[0]; // pointer to the data struct at address &memory[0]
    list->magic = MAGIC_FREE;
    list->size = memory_size;
    list->next = 0;
    list->prev = 0;
}





// Input: n - number of bytes requested
// Output: p - a pointer, or NULL
// Precondition: n is < size of memory available to the allocator
// Postcondition: If a region of size n or greater cannot be found, p = NULL 
//                Else, p points to a location immediately after a header block
//                      for a newly-allocated region of some size >= 
//                      n + header size.

void *vlad_malloc(u_int32_t n)
{
    // ----------------------------- INITIALISATION ----------------------------- //
    // given the size we want to allocate to memory + header size
    u_int32_t mallocSize = memoryInit(n + HEADER_SIZE);
    
    // Return NULL if required size is larger than available.
    if (mallocSize > memory_size) {return NULL;}
    
    // Initialise the current pointer to the first free memory space
    free_header_t *curr = convertIndex(free_list_ptr);

    // ----------------------------- FIRST ALLOC ----------------------------- //
    // IF the current memory is undivided.
    if (curr->size == memory_size) {
        
        // The count is for the fact we'll be allocating the first node. We'll use the count in the below if statement.
        int count = 0;
        int indexOfLastNode = 0;
        // Node will be halved continually until the size is the smallest possible
        while (curr->size/2 >= mallocSize) {
            halveNode(curr);
            // When we first halve the node, we get the 2nd half of the whole memory chunk, and make sure it's properly pointing to the node after the alloc'd node
            if (count == 0) {
                convertIndex(curr->next)->next = mallocSize;
                indexOfLastNode = curr->next;
            }
            count++;
        }
        // Set the remaining variables
        free_list_ptr = curr->next;
        curr->magic = MAGIC_ALLOC;
        convertIndex(curr->next)->prev = indexOfLastNode;
        
        return (void *)((byte *)curr + HEADER_SIZE);
    }

    // ----------------------------- TRAVERSE ----------------------------- //
    int counter;
    // Traverse through the list to find any available memory sizes
    for (counter = 0; curr->next != 0; curr = convertIndex(curr->next)) {
        // If the size is large enough for the malloc, then exit the loop
        if (curr->size >= mallocSize) {
            break;
        }
        counter++;
    }
    
//    printf("Traverse: Counter = %d\n",counter);
    
    // ----------------------------- ITERATION CASE 1 ----------------------------- //
    // Checks IF we (iterated through the node zero times OR iterated until the end) & IF the size request was too large
    if ((curr->next == 0 || counter == 0) && curr->size < mallocSize) {
//        printf("IT C1: \n");
        printf("The Size request was too big\n");
        return NULL;
        
    // ----------------------------- ITERATION CASE 2 ----------------------------- //
    // Checks IF we iterated zero times & the size was large enough to malloc
    } else if (counter == 0 && curr->size >= mallocSize){
//        printf("IT C2: \n");
        // Node will be halved continually until the size is the smallest possible
        while (curr->size/2 >= mallocSize) {
            halveNode(curr);
        }
        
        // ----------------------------- CASE 1 ----------------------------- //
        // Checks IF we're dealing with the first node
        if (convertPointer(curr) == free_list_ptr) {
//            printf("C1: \n");
            free_header_t *nodePoint;
            // Traverse to through the free nodes to the last node
            for (nodePoint = convertIndex(free_list_ptr); nodePoint->next != free_list_ptr; nodePoint = convertIndex(nodePoint->next)) {}
            
            // free_list_ptr changes if the current first node is being alloc'd
            free_list_ptr = curr->next;
            // Adjust the new first node
            convertIndex(free_list_ptr)->prev = convertPointer(nodePoint);
            // Adjust the last node to point to the new first node
            nodePoint->next = free_list_ptr;
            curr->magic = MAGIC_ALLOC;
            return (void *)((byte *)curr + HEADER_SIZE);
            
        // ----------------------------- CASE 2 ----------------------------- //
        // IF we're not dealing with the first node, make sure that the node before  and after the one being malloced point to the right nodes
        } else {
//            printf("C2: \n");
            
            // Create a pointer to the node before curr
            free_header_t *alt1 = convertIndex(curr->prev);
            // Set its "next" to the node after curr
            alt1->next = curr->next;
            
            // Create a pointer to the node after curr
            free_header_t *alt2 = convertIndex(curr->next);
            // Set its "prev" to the node before curr
            alt2->prev = convertPointer(alt1);
            
            // We do NOT change the free_list_ptr here as we don't malloc the first node.
            curr->magic = MAGIC_ALLOC;
            return (void *)((byte *)curr + HEADER_SIZE);
        }
    
    // ----------------------------- ITERATION CASE 3 ----------------------------- //
    // Checks IF we iterated one or more times
    } else if (counter > 0){
//        printf("IT C3: \n");
        
        // Node will be halved continually until the size is the smallest possible
        while (curr->size/2 >= mallocSize) {
            halveNode(curr);
        }

        // Create a pointer to the node before curr
        free_header_t *alt1 = convertIndex(curr->prev);
        // Set its "next" to the node after curr
        alt1->next = curr->next;
        
        // Create a pointer to the node after curr
        free_header_t *alt2 = convertIndex(curr->next);
        // Set its "prev" to the node before curr
        alt2->prev = convertPointer(alt1);
        
        // We do NOT change the free_list_ptr here as we don't malloc the first node.
        curr->magic = MAGIC_ALLOC;
        return (void *)((byte *)curr + HEADER_SIZE);
        
    }
    return NULL;
}




// Input: object, a pointer.
// Output: none
// Precondition: object points to a location immediately after a header block
//               within the allocator's memory.
// Postcondition: The region pointed to by object can be re-allocated by 
//                vlad_malloc

void vlad_free(void *object)
{
    
    // ----------------------------- INITIALISATION ----------------------------- //
    vlink_t indexHeader = convertPointer(object) - HEADER_SIZE;
    free_header_t *pointerHeader = convertIndex(indexHeader);
    
    
    // ----------------------------- TEST ----------------------------- //
    // making sure the node at hand is actually malloc'd to begin with
    if (pointerHeader->magic == MAGIC_FREE){
        fprintf(stderr, "Attempt to free non-allocated memory");
        abort();
    } else if (pointerHeader->magic == MAGIC_ALLOC){
        pointerHeader->magic = MAGIC_FREE;
        
        vlink_t indexToNode;
        
        int testCounter = 0;
        for (indexToNode = free_list_ptr; indexToNode < indexHeader; indexToNode = convertIndex(indexToNode)->next) {
            if (convertIndex(indexToNode)->next == free_list_ptr) {
                testCounter = 1;
                break;
            }
        }
        
        // ----------------------------- CASE 1 ----------------------------- //
        // Case where we're dealing with the last node
        if (testCounter == 1) {
//            printf("CASE 1\n");
            
            // ----------------------------- TRAVERSE ----------------------------- //
            // traverse until the node after the node being freed
            for (indexToNode = free_list_ptr;
                 convertIndex(indexToNode)->next != free_list_ptr;
                 indexToNode = convertIndex(indexToNode)->next) {}
            
            // ----------------------------- INITIALISATION ----------------------------- //
            convertIndex(free_list_ptr)->prev = indexHeader;
            convertIndex(indexToNode)->next = indexHeader;
            pointerHeader->prev = indexToNode;
            pointerHeader->next = free_list_ptr;
            
        // ----------------------------- CASE 2 ----------------------------- //
        // Case we're NOT dealing with the last node
        } else {
//            printf("CASE 2\n");
            
            // ----------------------------- TRAVERSE ----------------------------- //
            // traverse until the node after the node being freed
            for (indexToNode = free_list_ptr;
                 indexToNode < indexHeader;
                 indexToNode = convertIndex(indexToNode)->next) {}
            
            // ----------------------------- INITIALISATION ----------------------------- //
            // Create a pointer to the node before chosen node
            free_header_t *previousNode = convertIndex(convertIndex(indexToNode)->prev);
            
            // Create a pointer to the node after chosen node
            free_header_t *nextNode = convertIndex(indexToNode);
            
            previousNode->next = nextNode->prev = indexHeader;
            pointerHeader->next = convertPointer(nextNode);
            pointerHeader->prev = convertPointer(previousNode);
            
            // ----------------------------- CASE 3 ----------------------------- //
            // Case we're dealing with the node before free_list_ptr
            if (indexHeader < free_list_ptr) {
//                printf("CASE 3\n");
                free_list_ptr = indexHeader;
            }
        }
    }
    mergeNode();
}


// Stop the allocator, so that it can be init'ed again:
// Precondition: allocator memory was once allocated by vlad_init()
// Postcondition: allocator is unusable until vlad_int() executed again

void vlad_end(void)
{
    if (memory != NULL) {
        free(memory);
    } else {
        fprintf(stderr, "No memory has been initialised\n");
    }
}





// Precondition: allocator has been vlad_init()'d
// Postcondition: allocator stats displayed on stdout

void vlad_stats(void)
{
    printf("Statistics being generated. Please wait.\nLapu Lapu Lapu Dots.\nNapu Napu Napu Nots.\n");
    printf("- Rick Sanchez\n");
    
    printf("\nFreeListPtr = %d \n\n", free_list_ptr);
    free_header_t *nodePointer;
    int counter = 1;
    char *magicString;
    for (nodePointer = convertIndex(free_list_ptr); nodePointer->next != free_list_ptr; nodePointer = convertIndex(nodePointer->next)) {
        
        if (nodePointer->magic == MAGIC_FREE) {
            magicString = "MAGIC FREE";
        } else {
            magicString = "MAGIC ALLOC";
        }
        printf("------ Current Node %d ------\nIndex = %d\n",counter,convertPointer(nodePointer));
        printf("prev = %d    next = %d\nsize = %d\nmagic = %s\n\n",nodePointer->prev,nodePointer->next,nodePointer->size,magicString);
        if (nodePointer->magic != MAGIC_FREE) {
            printf("NOT FREE!! ABORT!!! ABORT!!! ABORT!!! ABORT!!! \n");
            abort();
        }
        counter++;
    }
    if (nodePointer->magic == MAGIC_FREE) {
        magicString = "MAGIC FREE";
    } else {
        magicString = "MAGIC ALLOC";
    }
    printf("------ Current Node %d ------\nIndex = %d\n",counter,convertPointer(nodePointer));
    printf("prev = %d    next = %d\nsize = %d\nmagic = %s\n\n",nodePointer->prev,nodePointer->next,nodePointer->size,magicString);
    if (nodePointer->magic != MAGIC_FREE) {
        printf("NOT FREE!! ABORT!!! ABORT!!! ABORT!!! ABORT!!! \n");
        abort();
    }
   return;
}



void mergeNode (void){
    free_header_t *nodePointer;
    int changes = TRUE;
    //while changes have occurred
    for (changes = TRUE; changes != FALSE;) {
        //sets this to false. if we never enter the "mergeNow == 1" statement, it means we did not find anything to merge, thus we stop this loop.
        changes = FALSE;
        //traverse to the end node
        for (nodePointer = convertIndex(free_list_ptr); nodePointer->next != free_list_ptr; nodePointer = convertIndex(nodePointer->next)) {
            
            // if the next node position is right next to the current node
            if (nodePointer->next == convertPointer(nodePointer) + nodePointer->size) {
                // if the current node size is the same as the next node size
                if (nodePointer->size == convertIndex(nodePointer->next)->size) {
                    int mergeNow = 1;
                    
                    //we can do this test until we get to the 2nd last node
                    if (convertIndex(nodePointer->next)->next != free_list_ptr) {
                        //if (from the current node) the next->next node is the same size as the next node we move on.
                        if (convertIndex(nodePointer->next)->size == convertIndex(convertIndex(nodePointer->next)->next)->size && convertIndex(nodePointer->next)->next != free_list_ptr) {
                            // we dun merge
                            mergeNow = 0;
                        } else {
                            //we merge
                            mergeNow = 1;
                        }
                    }
                    
                    // MERGE if we should
                    if (mergeNow == 1) {
                        free_header_t *adjNode = convertIndex(nodePointer->next);
                        vsize_t totalSize = nodePointer->size + adjNode->size;
                        nodePointer->size = totalSize;
                        nodePointer->next = adjNode->next;
                        convertIndex(adjNode->next)->prev = convertPointer(nodePointer);
                        changes = TRUE;
                    }
                }
            }
        }
    }
}


void halveNode(free_header_t *curr) {
    if (curr->magic != MAGIC_FREE) {
        fprintf(stderr, "Memory corruption");
        abort();
    }
    
    int halfWayIndex;
    // if we're dealing with the last node
    if (curr->next != free_list_ptr){
        // finds the halfway point if the memory can be halved.
        halfWayIndex = curr->next - (curr->size / 2);
        
    // if we're not dealing with the last node
    } else {
        // finds the halfway point if the memory can be halved.
        halfWayIndex = memory_size - (curr->size / 2);
    }
    
    // alters the current memory node to point to the halfway point.
    curr->size /= 2;
    
    // initialise the 2nd half of the Halved Memory.
    free_header_t *alt1 = convertIndex(halfWayIndex);
    alt1->magic = MAGIC_FREE;
    alt1->size = curr->size;
    alt1->next = curr->next; // .......(1)
    alt1->prev = convertPointer(curr);
    
    // initialise the node which is after the 2nd half node.
    if (alt1->next != free_list_ptr) {
        free_header_t *alt2 = convertIndex(alt1->next);
        alt2->prev = halfWayIndex;
    }
    
    // NOTE: this is after the 2nd half of the node is initialised as (1) requires for it to point to curr->next before the change is made.
    curr->next = halfWayIndex;
    
}

//returns the size of the next pow 2 for memory allocation
u_int32_t memoryInit (u_int32_t size){
    u_int32_t counter = 0;
    for (counter = 2; counter < size; counter *= 2);
    return counter;
}

//finds out how many times you have to divide the memory by 2
u_int32_t degreeTwo (u_int32_t size, u_int32_t memory){
    int counter = 0;
    for (counter = 0; memory/2 >= size; counter++) memory /= 2;
    return counter;
}

//converts a pointer into a corresponding index value
free_header_t *convertIndex (vaddr_t index){
    return (free_header_t *)&memory[index];
}

//converts an index into the corresponding pointer value
vaddr_t convertPointer (free_header_t *pointer){
    // the difference in the two addresses will return the index
    return (byte *)pointer - memory;
}




//
// All of the code below here was written by Alen Bou-Haidar, COMP1927 14s2
//

//
// Fancy allocator stats
// 2D diagram for your allocator.c ... implementation
// 
// Copyright (C) 2014 Alen Bou-Haidar <alencool@gmail.com>
// 
// FancyStat is free software: you can redistribute it and/or modify 
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or 
// (at your option) any later version.
// 
// FancyStat is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>


#include <string.h>

#define STAT_WIDTH  32
#define STAT_HEIGHT 16
#define BG_FREE      "\x1b[48;5;35m" 
#define BG_ALLOC     "\x1b[48;5;39m"
#define FG_FREE      "\x1b[38;5;35m" 
#define FG_ALLOC     "\x1b[38;5;39m"
#define CL_RESET     "\x1b[0m"


typedef struct point {int x, y;} point;

static point offset_to_point(int offset,  int size, int is_end);
static void fill_block(char graph[STAT_HEIGHT][STAT_WIDTH][20], 
                        int offset, char * label);



// Print fancy 2D view of memory
// Note, This is limited to memory_sizes of under 16MB
void vlad_reveal(void *alpha[26])
{
    int i, j;
    vlink_t offset;
    char graph[STAT_HEIGHT][STAT_WIDTH][20];
    char free_sizes[26][32];
    char alloc_sizes[26][32];
    char label[3]; // letters for used memory, numbers for free memory
    int free_count, alloc_count, max_count;
    free_header_t * block;

//	// TODO
//	// REMOVE these statements when your vlad_malloc() is done
//    printf("vlad_reveal() won't work until vlad_malloc() works\n");
//    return;

    // initilise size lists
    for (i=0; i<26; i++) {
        free_sizes[i][0]= '\0';
        alloc_sizes[i][0]= '\0';
    }

    // Fill graph with free memory
    offset = 0;
    i = 1;
    free_count = 0;
    while (offset < memory_size){
        block = (free_header_t *)(memory + offset);
        if (block->magic == MAGIC_FREE) {
            snprintf(free_sizes[free_count++], 32, 
                "%d) %d bytes", i, block->size);
            snprintf(label, 3, "%d", i++);
            fill_block(graph, offset,label);
        }
        offset += block->size;
    }

    // Fill graph with allocated memory
    alloc_count = 0;
    for (i=0; i<26; i++) {
        if (alpha[i] != NULL) {
            offset = ((byte *) alpha[i] - (byte *) memory) - HEADER_SIZE;
            block = (free_header_t *)(memory + offset);
            snprintf(alloc_sizes[alloc_count++], 32, 
                "%c) %d bytes", 'a' + i, block->size);
            snprintf(label, 3, "%c", 'a' + i);
            fill_block(graph, offset,label);
        }
    }

    // Print all the memory!
    for (i=0; i<STAT_HEIGHT; i++) {
        for (j=0; j<STAT_WIDTH; j++) {
            printf("%s", graph[i][j]);
        }
        printf("\n");
    }

    //Print table of sizes
    max_count = (free_count > alloc_count)? free_count: alloc_count;
    printf(FG_FREE"%-32s"CL_RESET, "Free");
    if (alloc_count > 0){
        printf(FG_ALLOC"%s\n"CL_RESET, "Allocated");
    } else {
        printf("\n");
    }
    for (i=0; i<max_count;i++) {
        printf("%-32s%s\n", free_sizes[i], alloc_sizes[i]);
    }

}

// Fill block area
static void fill_block(char graph[STAT_HEIGHT][STAT_WIDTH][20], 
                        int offset, char * label)
{
    point start, end;
    free_header_t * block;
    char * color;
    char text[3];
    block = (free_header_t *)(memory + offset);
    start = offset_to_point(offset, memory_size, 0);
    end = offset_to_point(offset + block->size, memory_size, 1);
    color = (block->magic == MAGIC_FREE) ? BG_FREE: BG_ALLOC;

    int x, y;
    for (y=start.y; y < end.y; y++) {
        for (x=start.x; x < end.x; x++) {
            if (x == start.x && y == start.y) {
                // draw top left corner
                snprintf(text, 3, "|%s", label);
            } else if (x == start.x && y == end.y - 1) {
                // draw bottom left corner
                snprintf(text, 3, "|_");
            } else if (y == end.y - 1) {
                // draw bottom border
                snprintf(text, 3, "__");
            } else if (x == start.x) {
                // draw left border
                snprintf(text, 3, "| ");
            } else {
                snprintf(text, 3, "  ");
            }
            sprintf(graph[y][x], "%s%s"CL_RESET, color, text);            
        }
    }
}

// Converts offset to coordinate
static point offset_to_point(int offset,  int size, int is_end)
{
    int pot[2] = {STAT_WIDTH, STAT_HEIGHT}; // potential XY
    int crd[2] = {0};                       // coordinates
    int sign = 1;                           // Adding/Subtracting
    int inY = 0;                            // which axis context
    int curr = size >> 1;                   // first bit to check
    point c;                                // final coordinate
    if (is_end) {
        offset = size - offset;
        crd[0] = STAT_WIDTH;
        crd[1] = STAT_HEIGHT;
        sign = -1;
    }
    while (curr) {
        pot[inY] >>= 1;
        if (curr & offset) {
            crd[inY] += pot[inY]*sign; 
        }
        inY = !inY; // flip which axis to look at
        curr >>= 1; // shift to the right to advance
    }
    c.x = crd[0];
    c.y = crd[1];
    return c;
}
